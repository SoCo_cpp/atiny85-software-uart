
PROJECT=test
MCU=attiny85
AVRDUDE_PROGRAMMER_ID=usbasp
AVRDUDE_PROGRAMMER_MCU=t85
# use s (size opt), 1, 2, 3 or 0 (off)
OPT_LEVEL=0

HEXFORMAT=ihex
CXX=avr-g++
CC=avr-gcc
OBJCOPY=avr-objcopy
OBJDUMP=avr-objdump
SIZE=avr-size
LD=avr-ld
AVRDUDE=avrdude
REMOVE=rm -f

SRC=main.c softuart.c

INCLUDE_PATHS=/usr/lib/avr/include

LIBS=

# math library
#LIBS+=-lm

GDB_INIT_FILE=gdbinit-$(PROJECT)
TARGET_ELF=$(PROJECT).elf
TARGET_HEX=$(PROJECT).hex
TARGET_HEX_EE=$(PROJECT).ee.hex
TARGET_ASM=$(PROJECT).asm
TARGET_S=$(PROJECT).S


CFLAGS=-I. $(INCLUDE_PATHS) -Wall -gdwarf-2 -mmcu=$(MCU) -O$(OPT_LEVEL) \
		-fpack-struct -fshort-enums -funsigned-bitfields 
		#-funsigned-char 
		# -Wstrict-prototypes 

CPPFLAGS=-fno-exceptions

ASMFLAGS= $(CFLAGS) -v -g -x assembler-with-cpp 

#-Wa,-gdwarf2
		
LDFLAGS=-Wall -Wl,-Map,$(TARGET_ELF).map -mmcu=$(MCU) $(LIBS)

C_LST_OUTPUT=-Wa,-ahlms=$(<:.c=.c.lst)
CPP_LST_OUTPUT=-Wa,-ahlms=$(<:.cpp=.cpp.lst)
ASM_LST_OUTPUT=-Wa,-gstabs 
#,-ahlms=$(<:.S=.S.lst)


SRC_CPP=$(filter %.cpp, $(SRC))
SRC_C=$(filter %.c, $(SRC)) 
SRC_ASM=$(filter %.S, $(SRC)) 

OBJDEPS=$(SRC_CPP:.cpp=.cpp.o) 	\
		$(SRC_C:.c=.c.o)     	\
		$(SRC_ASM:.S=.S.o)	

GEN_FILES_LST=$(OBJDEPS:.o=.lst) 

all: $(TARGET_S)
	@echo "Success"

#disasm: $(TARGET_S) stats

stats:  $(TARGET_ELF)
	$(OBJDUMP) -h $(TARGET_ELF)
	$(SIZE)  $(TARGET_ELF)

hex: $(TARGET_HEX) $(TARGET_HEX_EE)

writeflash: hex
		$(AVRDUDE) -c $(AVRDUDE_PROGRAMMER_ID) -p $(AVRDUDE_PROGRAMMER_MCU) -e -U flash:w:$(TARGET_HEX)

install: writeflash

$(TARGET_S): $(TARGET_ELF)
		$(OBJDUMP) -S $< > $@
		
$(TARGET_ELF): $(OBJDEPS)
			$(CC) $(LDFLAGS) -o $(TARGET_ELF) $(OBJDEPS)
	
# obj from C
%.c.o : %.c
	$(CC) $(CFLAGS) $(C_LST_OUTPUT) -c $< -o $@
# obj from C++
%.cpp.o : %.cpp
	$(CXX) $(CPPFLAGS) $(CPP_LST_OUTPUT) -c $< -o $@
# obj from ASM
%.S.o : %.S
	$(CC) $(ASMFLAGS) $(ASM_LST_OUTPUT) -c $< -o $@

	
%.hex : %.elf
	$(OBJCOPY) -j .text -j .data -O $(HEXFORMAT) $< $@

%.ee.hex : %.elf
	$(OBJCOPY) -j .eeprom --change-section-lma .eeprom=0 -O $(HEXFORMAT) $< $@
	
gdbinit: $(GDB_INIT_FILE)

$(GDB_INIT_FILE): $(TARGET_HEX)
	@echo "file $(TARGET_HEX)" 				> $(GDB_INIT_FILE)
	@echo "target remote localhost:1212" 	>> $(GDB_INIT_FILE)
	@echo "load"  						 	>> $(GDB_INIT_FILE)
	@echo "break main"  					>> $(GDB_INIT_FILE)
	@echo
	@echo "Use 'avr-gdb -x $(GDB_INIT_FILE)'"
	
dbg:
	@echo "Target S: $(TARGET_S)"
	@echo "Target ELF: $(TARGET_ELF)"
	@echo "Object deps: $(OBJDEPS)"
	@echo "File list: $(GEN_FILES_LST)"

clean:
	$(REMOVE) $(TARGET_ELF) $(TARGET_ELF).map 
	$(REMOVE) $(OBJDEPS)
	$(REMOVE) $(GEN_FILES_LST)
	$(REMOVE) $(GDB_INIT_FILE)
	$(REMOVE) $(TARGET_HEX_EE) $(TARGET_HEX)
	$(REMOVE) $(TARGET_S)
