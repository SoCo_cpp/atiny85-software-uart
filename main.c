//------------------------------------------------------
/*
 ;                                ATtiny 25/45/85
 ; 
 ;         (PCINT5/RESET/ADC0/dW) PB5 1 [u] 8 VCC
 ;  (PCINT3/XTAL1/CLK1/OC1B/ADC3) PB3 2 [ ] 7 PB2 (SCK/USCK/SCL/ADC1/T0/INT0/PCINT2)
 ;  (PCINT4/XTAL2/CLK0/OC1B/ADC2) PB4 3 [ ] 6 PB1 (MISO/DO/Ain1/OC0B/OC1A/PCINT1)
 ;                                GND 4 [_] 5 PB0 (MOSI/DI/SDA/AIN0/OC0A/OC1A/AREF/PCINT0)
 */
//------------------------------------------------------
#define F_CPU 1000000 // 8 MHz
//------------------------------------------------------
#include <util/delay_basic.h>
#include <avr/io.h>
#include <softuart.h>
//------------------------------------------------------

//------------------------------------------------------

//------------------------------------------------------
#ifndef cbi
	#define cbi(byte, bit) (byte &= ~_BV(bit)) // (byte ^= (byte &  _BV(bit))) 
#endif
#ifndef sbi
	#define sbi(byte, bit) (byte |=  _BV(bit))
#endif
//------------------------------------------------------


//------------------------------------------------------
int main(void)
{
	//int i;
	int count = 0;
	//int wait = 0;
	unsigned char data[20];
	
	SHalfUARTConfig outCfg;
	SHalfUARTConfig inCfg;
	
	HalfUART_init_tx(&outCfg, PB2, 1200, 0);
	HalfUART_init_rx(&inCfg, PB4, 1200, 1);
	
	sbi(DDRB, PB0);
	cbi(PORTB, PB0);
	while (1)
	{
		//count = HalfUART_readline_ch(&inCfg, data, 6, '\n');
		count = HalfUART_readline_str(&inCfg, data, 20, "\r\n");
		//count = HalfUART_read(&inCfg, data, 20);
		if (count)
			HalfUART_write(&outCfg, data, count);
		
	}
	return 0;
} // main
//------------------------------------------------------
