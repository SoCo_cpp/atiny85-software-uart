//------------------------------------------------------
#include <softuart.h>
//------------------------------------------------------
void HalfUART_init_tx(SHalfUARTConfig* pCfg, unsigned char pin, unsigned int baud, unsigned char polarity)
{
	pCfg->bit = pin;
	switch (baud)
	{
		case 1200:
			pCfg->delayWriteCycles	= Uart_Baud_Delay_Write_Cycles_1200;
			break;
		case 2400:
			pCfg->delayWriteCycles	= Uart_Baud_Delay_Write_Cycles_2400;
			break;
		case 4800:
			pCfg->delayWriteCycles	= Uart_Baud_Delay_Write_Cycles_4800;
			break;
		case 9600:
		default:
			pCfg->delayWriteCycles	= Uart_Baud_Delay_Write_Cycles_9600;
			break;
	} // switch (baud)

	if (polarity == 1)
	{
		pCfg->flags = HUART_FLAG_INVERTED;
		cbi(PORTB, pCfg->bit); // initially active
	}
	else
	{
		pCfg->flags = HUART_FLAG_IGNORE_NONE;
		sbi(PORTB, pCfg->bit); // initially active
	}
	sbi(DDRB, pCfg->bit); // set as output
}
//------------------------------------------------------
void HalfUART_init_rx(SHalfUARTConfig* pCfg, unsigned char pin, unsigned int baud, unsigned char polarity)
{
	pCfg->delayReadCycles = 3;
	pCfg->bit = pin;
	switch (baud)
	{
		case 1200:
			pCfg->delayReadCycles		= Uart_Baud_Delay_Read_Cycles_1200;
			pCfg->delayAndHalfCycles	= Uart_Baud_DelayAndHalf_Cycles_1200;
			pCfg->flags = HUART_FLAG_IGNORE_NONE;
			break;
		case 2400:
			pCfg->delayReadCycles		= Uart_Baud_Delay_Read_Cycles_2400;
			pCfg->delayAndHalfCycles	= Uart_Baud_DelayAndHalf_Cycles_2400;
			pCfg->flags = HUART_FLAG_IGNORE_STOP;
			break;
		case 4800:
			pCfg->delayReadCycles		= Uart_Baud_Delay_Read_Cycles_4800;
			pCfg->delayAndHalfCycles	= Uart_Baud_DelayAndHalf_Cycles_4800;
			pCfg->flags = HUART_FLAG_IGNORE_STOP;
			break;
		case 9600:
		default:
			pCfg->delayReadCycles		= Uart_Baud_Delay_Read_Cycles_9600;
			pCfg->delayAndHalfCycles	= Uart_Baud_DelayAndHalf_Cycles_9600;
			pCfg->flags = HUART_FLAG_KEEPUP | HUART_FLAG_IGNORE_STOP;
			break;
	} // switch (baud)

	if (polarity == 1)
		pCfg->flags |= HUART_FLAG_INVERTED;
	cbi(DDRB,  pCfg->bit); // set as input
	cbi(PORTB, pCfg->bit); // no internal pullup
}
//------------------------------------------------------
void HalfUART_putc(SHalfUARTConfig* pCfg, unsigned char txData)
{
	uint8_t bitMask = (1 << pCfg->bit);
	uint8_t bitcount = 8;
	uint8_t shadow = 0;
	uint16_t delaycounter = 0;
	if ( (pCfg->flags & HUART_FLAG_INVERTED) != 0) // inverted polarity
	{
 		__asm__ volatile (	
				"0: "			"ldi %[bitcount], 8"					"\n\t"
								"in %[shadow],0x18"						"\n\t" // initialize shadow
			"StartBit_%=: " 	"or %[shadow],%[bitmask]"				"\n\t" 	// set start bit 
								"out 0x18, %[shadow]"					"\n\t"
								"nop"						"\n\t"	// ballance out instructions from bit active/inactive
								"nop"						"\n\t"	// ballance out instructions from bit active/inactive
								"nop"						"\n\t"	// ballance out instructions from bit active/inactive
								"nop"						"\n\t"	// ballance out instructions from bit active/inactive
								"movw %[delaycounter],%[delaycycles]"	"\n\t" // bit delay for start bit
			"StartDelay_%=: "	"sbiw %[delaycounter],1"				"\n\t"
								"brne StartDelay_%="						"\n\t"
			"BitLoop_%=: "   	"ror %[data]"							"\n\t" // rotate txData right, moving lowest bit in carry flag
								"brcc ActiveBit_%="						"\n\t" // branch if carry set to Set_xxx
								"mov __tmp_reg__,%[shadow]" 		"\n\t"	
								"and __tmp_reg__,%[bitmask]"		"\n\t" 	// check bitMask is set on tmp of port
								"brbs 1, BitNoChange_%="				"\n\t"	// skip if zero set
									"eor %[shadow],%[bitmask]"		"\n\t" 	// bit is on, flip port bit off
									"out 0x18,%[shadow]"			"\n\t"
									"rjmp BitDone_%="				"\n\t"
			"BitNoChange_%=: "	"nop"							"\n\t"
								"nop"							"\n\t"
								"nop"							"\n\t"
			"BitDone_%=: "		"movw %[delaycounter],%[delaycycles]"	"\n\t"
			"BitDelay_%=: "		"sbiw %[delaycounter],1"				"\n\t"
								"brne BitDelay_%="					"\n\t"
								"dec %[bitcount]"					"\n\t" 	// bit done, decrement bit counter
								"brne BitLoop_%="				"\n\t" 	// go back and get next bit
								"rjmp Done_%="					"\n\t" 	// no more bits go to done
			"ActiveBit_%=: "    "or %[shadow],%[bitmask]"	"\n\t" 	// current bit is active, set port bit on
								"out 0x18, %[shadow]"		"\n\t"
								"nop"						"\n\t"	// ballance out instructions from bit active/inactive
								"nop"						"\n\t"	// ballance out instructions from bit active/inactive
								"nop"						"\n\t"	// ballance out instructions from bit active/inactive
								"nop"						"\n\t"	// ballance out instructions from bit active/inactive
								"rjmp BitDone_%="			"\n\t"	// go back to bit done
			"Done_%=: "			"mov __tmp_reg__,%[shadow]" 			"\n\t" // copy shadow for test
								"and __tmp_reg__,%[bitmask]"			"\n\t" // get and of shadow and bitmask
								"brbs 1, StopBitDone_%="				"\n\t" // skip if zero set
									"eor %[shadow],%[bitmask]"			"\n\t" 	// bit is on, flip port bit off
									"out 0x18,%[shadow]"				"\n\t"
			"StopBitDone_%=: " 	"movw %[delaycounter],%[delaycycles]"	"\n\t" // bit delay for start bit
			"StopDelay_%=: "	"sbiw %[delaycounter],1"				"\n\t"
								"brne StopDelay_%="						"\n\t"
			"StopDone_%=: "		""										"\n\t"
								
							: // no outputs 
							: [data] "r" (txData), [port] "I" (_SFR_IO_ADDR(PORTB)), [bitmask] "r" (bitMask), [shadow] "r" (shadow), [bitcount] "r" (bitcount), [delaycycles] "w" (pCfg->delayWriteCycles), [delaycounter] "w" (delaycounter));
	}
	else // standard polarity
	{
		__asm__ volatile (	
				"0: "			"ldi %[bitcount], 8"					"\n\t"
								"in %[shadow],0x18"						"\n\t" // initialize shadow
			"StartBit_%=: "		"mov __tmp_reg__,%[shadow]" 			"\n\t" // copy shadow for test
								"and __tmp_reg__,%[bitmask]"			"\n\t" // get and of shadow and bitmask
								"brbs 1, StartBitDone_%="				"\n\t" // skip if zero set
									"eor %[shadow],%[bitmask]"			"\n\t" 	// bit is on, flip port bit off
									"out 0x18,%[shadow]"				"\n\t"
			"StartBitDone_%=: " "movw %[delaycounter],%[delaycycles]"	"\n\t" // bit delay for start bit
			"StartDelay_%=: "	"sbiw %[delaycounter],1"				"\n\t"
								"brne StartDelay_%="						"\n\t"
			"BitLoop_%=: "   	"ror %[data]"							"\n\t" // rotate txData right, moving lowest bit in carry flag
								"brcs ActiveBit_%="						"\n\t" // branch if carry set to Set_xxx
								"mov __tmp_reg__,%[shadow]" 		"\n\t"	
								"and __tmp_reg__,%[bitmask]"		"\n\t" 	// check bitMask is set on tmp of port
								"brbs 1, BitNoChange_%="				"\n\t"	// skip if zero set
									"eor %[shadow],%[bitmask]"		"\n\t" 	// bit is on, flip port bit off
									"out 0x18,%[shadow]"			"\n\t"
									"rjmp BitDone_%="				"\n\t"
			"BitNoChange_%=: "	"nop"							"\n\t"
								"nop"							"\n\t"
								"nop"							"\n\t"
			"BitDone_%=: "		"movw %[delaycounter],%[delaycycles]"	"\n\t"
			"BitDelay_%=: "		"sbiw %[delaycounter],1"				"\n\t"
								"brne BitDelay_%="					"\n\t"
								"dec %[bitcount]"					"\n\t" 	// bit done, decrement bit counter
								"brne BitLoop_%="				"\n\t" 	// go back and get next bit
								"rjmp Done_%="					"\n\t" 	// no more bits go to done
			"ActiveBit_%=: "    "or %[shadow],%[bitmask]"	"\n\t" 	// current bit is active, set port bit on
								"out 0x18, %[shadow]"		"\n\t"
								"nop"						"\n\t"	// ballance out instructions from bit active/inactive
								"nop"						"\n\t"	// ballance out instructions from bit active/inactive
								"nop"						"\n\t"	// ballance out instructions from bit active/inactive
								"nop"						"\n\t"	// ballance out instructions from bit active/inactive
								"rjmp BitDone_%="			"\n\t"	// go back to bit done
			"Done_%=: " 		"or %[shadow],%[bitmask]"	"\n\t" 	// set stop bit state
								"out 0x18, %[shadow]"		"\n\t"
								"nop"						"\n\t"	// ballance out instructions from bit active/inactive
								"nop"						"\n\t"	// ballance out instructions from bit active/inactive
								"nop"						"\n\t"	// ballance out instructions from bit active/inactive
								"nop"						"\n\t"	// ballance out instructions from bit active/inactive
								"movw %[delaycounter],%[delaycycles]"	"\n\t" // bit delay for start bit
			"StopDelay_%=: "	"sbiw %[delaycounter],1"				"\n\t"
								"brne StopDelay_%="						"\n\t"
								
							: // no outputs 
							: [data] "r" (txData), [port] "I" (_SFR_IO_ADDR(PORTB)), [bitmask] "r" (bitMask), [shadow] "r" (shadow), [bitcount] "r" (bitcount), [delaycycles] "w" (pCfg->delayWriteCycles), [delaycounter] "w" (delaycounter));
	}
}
//------------------------------------------------------
unsigned char HalfUART_getc(SHalfUARTConfig* pCfg)
{
	uint8_t rxData = 1;
	uint8_t bitMask	= (1 << pCfg->bit);
	uint8_t bitcount = 8;
	uint16_t delaycounter = 0;
	uint16_t startwaitcycles = ((unsigned int)pCfg->readWaitChars) * pCfg->delayReadCycles * ((unsigned int)10); // 10 bits, but double the cycles per loop
	if ( (pCfg->flags & HUART_FLAG_INVERTED) != 0) // inverted polarity (idle=low, start=high, bit active=high, stop= low)
	{
		if ( (pCfg->flags & HUART_FLAG_KEEPUP) == 0)
		{
			__asm__ volatile (
							"in __tmp_reg__, 0x16"			"\n\t"
							"and __tmp_reg__, %[bitmask]" 	"\n\t"
							"brne Fail_KU%="				"\r\n"
			"Success_KU%=: " "ldi %[data],1"				"\n\t"
							"rjmp Done_KU%="				"\n\t"
			"Fail_KU%=: " 	"ldi %[data],2" 				"\n\t"
							"rjmp Done_KU%="				"\n\t"
			"Done_KU%=: " 	"" 								"\n\t"
							: [data] "=r" (rxData)
							:  "0" (rxData), [bitmask] "r" (bitMask));
						
			if (rxData != 1/*has data*/)
			{
				pCfg->status = rxData; // error
				return 0; // Start bit detect fail
			}
		}
		
		__asm__ volatile (	// Z bit when: Active = S(low), Inactive = C(high)
							"movw %[delaycounter], %[startwaitcycles]"	"\n\t"
		"WaitStart_%=: "	"in __tmp_reg__, 0x16"						"\n\t"
							"and __tmp_reg__, %[bitmask]" 				"\n\t"
							"brne Start_%="			"\r\n"
							"sbiw %[delaycounter],1"					"\n\t"
							"brne WaitStart_%="							"\n\t"
							"rjmp None_A%="								"\n\n"
			"Start_%=: "	"in __tmp_reg__, 0x16"						"\n\t"
							"and __tmp_reg__, %[bitmask]" 				"\n\t"
							"brbs 1, Fail_A2%=" 						"\n\t" // fail on active, start bit should be inactive
							"nop" 										"\n\t"
							"movw %[delaycounter],%[dalayandhalfcycles]"	"\n\t" // bit delay for start bit, and half way into next bit
		"StartDelay_%=: "	"sbiw %[delaycounter],1"					"\n\t"
							"brne StartDelay_%="						"\n\t"
							"rjmp Success_A%="								"\n\t"
			"Success_A%=: " "ldi %[data],1"							"\n\t"
							"rjmp Done_A%="		"\n\t"
			"Fail_A2%=: " 	"ldi %[data],3" 						"\n\t"
							"rjmp Done_A%="		"\n\t"
			"None_A%=: " 	"ldi %[data],0" 						"\n\t"
			"Done_A%=: " 	"" 						"\n\t"
							: [data] "=r" (rxData)
							:  "0" (rxData), [bitmask] "r" (bitMask), [dalayandhalfcycles] "w" (pCfg->delayAndHalfCycles), [delaycounter] "w" (delaycounter), [startwaitcycles] "w" (startwaitcycles));
		pCfg->status = rxData; // error or none
		if (rxData != 1/*has data*/)
			return 0; // Start bit detect fail
							
		__asm__ volatile (	 
			"BitLoop_%=: "	""											"\n\t"
							"in __tmp_reg__, 0x16"						"\n\t"
							"and __tmp_reg__, %[bitmask]" 				"\n\t"
							"brbs 1, BitActive_%=" 						"\n\t" // check for high bit
							"clc"										"\n\t"
							"rjmp BitSave_%="							"\n\t"
		"BitActive_%=: "	"sec"										"\n\t"
							"rjmp BitSave_%="							"\n\t"
		"BitSave_%=: "		"ror %[data]"								"\n\t"
			"BitDelay_%=: "	"dec %[bitcount]"							"\n\t" 	// bit done, decrement bit counter
							"breq Success_%="							"\n\t" 	// (ignore stop) skip delay and stop test if got all bits
							"movw %[delaycounter],%[delaycycles]"		"\n\t"
		"BitDelayLoop_%=: "	"sbiw %[delaycounter],1"					"\n\t"
							"brne BitDelayLoop_%="						"\n\t"
							"rjmp BitLoop_%="							"\n\t"
			"Fail_%=: " 	"ldi %[data],0" 							"\n\t"
							"ldi %[status],4"							"\n\t"
							"rjmp Done_%="								"\n\t"
			"Success_%=: " 	"ldi %[status],1"							"\n\t"
			"Done_%=: " 	"" 											//"\n\t"
							: [data] "=r" (rxData), [status] "=r" (pCfg->status)
							: "0" (rxData), "1" (pCfg->status), [bitmask] "r" (bitMask), [bitcount] "r" (bitcount), [delaycycles] "w" (pCfg->delayReadCycles), [delaycounter] "w" (startwaitcycles));
		if ( (pCfg->flags & HUART_FLAG_IGNORE_STOP) == 0)
		{
			startwaitcycles = 120;
			__asm__  (	 
							"movw %[delaycounter], %[delaycycles]"	"\n\t"
			"ExtraLoop_%=:"	"sbiw %[delaycounter],1"				"\n\t"
							"brne ExtraLoop_%="						"\n\t"
							: 
							: [delaycounter] "w" (delaycounter), [delaycycles] "w" (startwaitcycles));
			
			
		}
	}
	else // standard polarity // if (pCfg->activePolarity) // inverted polarity
	{
		__asm__ volatile (	// Z bit when: Active = C(high), Inactive = S(low)
							"in __tmp_reg__, 0x16"						"\n\t"
							"and __tmp_reg__, %[bitmask]" 				"\n\t"
							"brbs 1, Fail_A%=" 							"\n\t" // fail on inactive, shoudle be active on idle, must be middle of byte 
							"movw %[delaycounter], %[startwaitcycles]"	"\n\t"
		"WaitStart_%=: "	"in __tmp_reg__, 0x16"						"\n\t"
							"and __tmp_reg__, %[bitmask]" 				"\n\t"
							"brbs 1, Start_%=" 							"\n\t" // start bit should go inactive
							"sbiw %[delaycounter],1"					"\n\t"
							"brne WaitStart_%="							"\n\t"
							"rjmp None_A%="								"\n\n"
			"Start_%=: "	"in __tmp_reg__, 0x16"						"\n\t"
							"and __tmp_reg__, %[bitmask]" 				"\n\t"
							"brbc 1, Fail_A%=" 							"\n\t" // fail on active, start bit should be inactive
							"nop" 						"\n\t"
							"movw %[delaycounter],%[dalayandhalfcycles]"	"\n\t" // bit delay for start bit, and half way into next bit
		"StartDelay_%=: "	"sbiw %[delaycounter],1"					"\n\t"
							"brne StartDelay_%="						"\n\t"
							"rjmp Done_A%="								"\n\t"
			"Fail_A%=: " 	"ldi %[data],2" 							"\n\t"
			"None_A%=: " 	"ldi %[data],0" 						"\n\t"
			"Done_A%=: " 	"" 						//"\n\t"
							: [data] "=r" (rxData)
							:  "0" (rxData), [pin] "I" (_SFR_IO_ADDR(PINB)), [bitmask] "r" (bitMask), [dalayandhalfcycles] "w" (pCfg->delayAndHalfCycles), [delaycounter] "w" (delaycounter), [startwaitcycles] "w" (startwaitcycles), [bit] "r" (pCfg->bit));
		pCfg->status = rxData; // error or none
		if (rxData != 1/*has data*/)
			return 0; // Start bit detect fail
							
		__asm__ volatile (	 
			"BitLoop_%=: "	"in __tmp_reg__, 0x16"						"\n\t"
							"and __tmp_reg__, %[bitmask]" 				"\n\t"
							"brbc 1, BitActive_%=" 						"\n\t" // check for active bit
							"clc"										"\n\t"
							"rjmp BitSave_%="							"\n\t"
		"BitActive_%=: "	"sec"										"\n\t"
							"rjmp BitSave_%="							"\n\t"
		"BitSave_%=: "		"ror %[data]"								"\n\t"
			"BitDelay_%=: "	"dec %[bitcount]"							"\n\t" 	// bit done, decrement bit counter
							"breq Done_%="								"\n\t" 	// (ignore stop) skip delay and stop test if got all bits
							"movw %[delaycounter],%[delaycycles]"		"\n\t"
		"BitDelayLoop_%=: "	"sbiw %[delaycounter],1"					"\n\t"
							"brne BitDelayLoop_%="						"\n\t"
							"rjmp BitLoop_%="							"\n\t"
			"Fail_%=: " 	"ldi %[data],0" 							"\n\t"
							"ldi %[status],2"							"\n\t"
			"Done_%=: " 	"" 											//"\n\t"
							: [data] "=r" (rxData), [status] "=r" (pCfg->status)
							: "0" (rxData), "1" (pCfg->status), [pin] "I" (_SFR_IO_ADDR(PINB)), [bitmask] "r" (bitMask), [bitcount] "r" (bitcount), [delaycycles] "w" (pCfg->delayReadCycles), [delaycounter] "w" (startwaitcycles));
		if ( (pCfg->flags & HUART_FLAG_IGNORE_STOP) == 0)
		{
			startwaitcycles = 120;
			__asm__  (	 
							"movw %[delaycounter], %[delaycycles]"	"\n\t"
			"ExtraLoop_%=:"	"sbiw %[delaycounter],1"				"\n\t"
							"brne ExtraLoop_%="						"\n\t"
							: 
							: [delaycounter] "w" (delaycounter), [delaycycles] "w" (startwaitcycles));
			
			
		}
	} // else // standard polarity // if (pCfg->activePolarity) // inverted polarity
	// Ignore delay for last half of stop bit for quickest return
	return rxData;
}
//------------------------------------------------------
unsigned int HalfUART_read(SHalfUARTConfig* pCfg, unsigned char* pbuffer, unsigned int maxlen)
{
	unsigned int len = 0;
	do 
	{
		pbuffer[len++] = HalfUART_getc(pCfg);
		if (!HalfUART_is_read_status(pCfg, HUART_STATUS_READ_DATA))
		{
			len--; // remove last failed increment
			break;
		}
	} while (len < maxlen);
	return len; // last character was successful, max reached	
}
//------------------------------------------------------
unsigned int HalfUART_readline_ch(SHalfUARTConfig* pCfg, unsigned char* pbuffer, unsigned int maxlen, unsigned char chDelimiter)
{
	unsigned int len = 0;
	unsigned char readWaitChars = pCfg->readWaitChars;
	pCfg->readWaitChars = 1;
	do 
	{
		pbuffer[len] = HalfUART_getc(pCfg);
		if (!HalfUART_is_read_status(pCfg, HUART_STATUS_READ_DATA))
			break; // no extra len increment
	} while (len < maxlen && pbuffer[len++] != chDelimiter);
	pbuffer[len] = '\0'; // ensure null terminator
	pCfg->readWaitChars = readWaitChars;
	return len; 
}
//------------------------------------------------------
unsigned int HalfUART_readline_str(SHalfUARTConfig* pCfg, unsigned char* pbuffer, unsigned int maxlen, unsigned char* strDelimiter)
{
		
	unsigned int len = 0;
	unsigned int delLen = 0;
	do 
	{
		pbuffer[len] = HalfUART_getc(pCfg);
		if (!HalfUART_is_read_status(pCfg, HUART_STATUS_READ_DATA))
		{
			pbuffer[len++]  = 'E';
			pbuffer[len++] = ('0' + pCfg->status);
			break; // error
		}
		if (pbuffer[len++] != strDelimiter[delLen])
			delLen = 0;
		else if (strDelimiter[++delLen] == '\0')
			break; // matched till null terminator
	} while (len < maxlen);
	pbuffer[len] = '\0'; // ensure null terminator
	return len; 
}
//------------------------------------------------------
void HalfUART_write(SHalfUARTConfig* pCfg, unsigned char* pbuffer, unsigned int len)
{
	unsigned int idx = 0;
	for (;idx < len;++idx)
		HalfUART_putc(pCfg, pbuffer[idx]);
}
//------------------------------------------------------
void HalfUART_puts(SHalfUARTConfig* pCfg, unsigned char* pbuffer)
{
	unsigned int idx = 0;
	while (pbuffer[idx])
		HalfUART_putc(pCfg, pbuffer[idx++]);
}
//------------------------------------------------------
