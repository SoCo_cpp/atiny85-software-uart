#ifndef _SOFT_UART_H
#define _SOFT_UART_H
//------------------------------------------------------
#define F_CPU 1000000 // 8 MHz
#include <avr/io.h>
//------------------------------------------------------
//#define SUART_DEBUG_OUT_TIMING
//#define SUART_DEBUG_IN_TIMING
//------------------------------------------------------
// Debug read pulses:
//  - begin 2
//  - start fail 3
//  - stop fail  4
//------------------------------------------------------
typedef struct
{
	uint8_t bit;
	unsigned int  delayReadCycles;
	unsigned int  delayWriteCycles;
	unsigned int  delayAndHalfCycles;
	unsigned char readWaitChars; // character's worth of time to wait for data
	unsigned char flags;
	unsigned char status; // successfully read character
} SHalfUARTConfig;
//------------------------------------------------------
#define HUART_STATUS_READ_NONE		0
#define HUART_STATUS_READ_DATA		1
#define HUART_STATUS_READ_ERROR		2
//------------------------------------------------------
#define HalfUART_set_status(c,s)  (c->status = s)
#define HalfUART_is_read_status(c,s) (c->status == s)
//------------------------------------------------------
#define HUART_FLAG_IGNORE_NONE				0
#define HUART_FLAG_IGNORE_STOP				1
#define HUART_FLAG_MULTIREAD_IGNORE_STOP	2
#define HUART_FLAG_INVERTED					4
#define HUART_FLAG_KEEPUP					8
//------------------------------------------------------
void HalfUART_config_tx(SHalfUARTConfig* pCfg, unsigned int baud);
void HalfUART_config_rx(SHalfUARTConfig* pCfg, unsigned int baud);
//------------------------------------------------------
void HalfUART_init_tx(SHalfUARTConfig* pCfg, unsigned char pin, unsigned int baud, unsigned char polarity);
void HalfUART_init_rx(SHalfUARTConfig* pCfg, unsigned char pin, unsigned int baud, unsigned char polarity);
//------------------------------------------------------
void HalfUART_putc(SHalfUARTConfig* pCfg, unsigned char txData);
unsigned char HalfUART_getc(SHalfUARTConfig* pCfg); 
//------------------------------------------------------
void HalfUART_puts(SHalfUARTConfig* pCfg, unsigned char* pbuffer);
void HalfUART_write(SHalfUARTConfig* pCfg, unsigned char* pbuffer, unsigned int len);
unsigned int HalfUART_read(SHalfUARTConfig* pCfg, unsigned char* pbuffer, unsigned int maxlen);
unsigned int HalfUART_readline_ch(SHalfUARTConfig* pCfg, unsigned char* pbuffer, unsigned int maxlen, unsigned char chDelimiter);
unsigned int HalfUART_readline_str(SHalfUARTConfig* pCfg, unsigned char* pbuffer, unsigned int maxlen, unsigned char* strDelimiter);
//------------------------------------------------------
//------------------------------------------------------
/*--------------------------------------------------
; 1000000 Mhz   0.225690276 cycles per uS
;--------------------------------------------------
; baud   bit time uS   x1.5       cycles at 8mhz         _delay_loop_2 full   part        full part
;--------------------------------------------------
; 9600   104           156         1711934   2567901	              26   7998             39  11997           
; 4800   208           312		   3423868   5135802	              52   15996            79  23994     
; 2400   416.6         624.9       6857613  10286420  	             104   41869           156  62804          
; 1200   833           1249.5     13711934  20567901	             209   14910           313  55133              
; _delay_loop_2 4 cycles per count
*-------------------------------------------------*/                                
//------------------------------------------------------
#define Uart_Baud_Delay_Read_Cycles_9600     23 // 23.471788715		T dif -1
#define Uart_Baud_Delay_Write_Cycles_9600    23 // 23.207683073 	T dif 0
#define Uart_Baud_DelayAndHalf_Cycles_9600   22 // 35.207683073		T dif -13
#define Uart_Baud_Delay_Write_Cycles_4800	 50 // 46.943577408 	T dif +4
#define Uart_Baud_Delay_Read_Cycles_4800	 47 // 46.943577408 	T dif +1
#define Uart_Baud_DelayAndHalf_Cycles_4800   68 // 70.415366112 	T dif +2
#define Uart_Baud_Delay_Write_Cycles_2400	103	// 94.022568982 	T dif +9
#define Uart_Baud_Delay_Read_Cycles_2400    102	// 94.022568982 	T dif +8
#define Uart_Baud_DelayAndHalf_Cycles_2400  146 // 141.033853472 	T dif -5
#define Uart_Baud_Delay_Write_Cycles_1200   208 // 208.25 			T dif 0
#define Uart_Baud_Delay_Read_Cycles_1200    208 // 208.25 			T dif 0
#define Uart_Baud_DelayAndHalf_Cycles_1200  310 // 312.375 			T dif -2
//------------------------------------------------------
//------------------------------------------------------
#ifndef cbi
	#define cbi(byte, bit) (byte &= ~_BV(bit)) // (byte ^= (byte &  _BV(bit))) 
#endif
#ifndef sbi
	#define sbi(byte, bit) (byte |=  _BV(bit))
#endif
//------------------------------------------------------
#if defined(SUART_DEBUG_OUT_TIMING) || defined(SUART_DEBUG_IN_TIMING)
	#define SUART_DEBUG_NOP_ASM					"nop\n\t"
#else
	#define SUART_DEBUG_NOP_ASM
#endif
//------------------------------------------------------
#ifdef SUART_DEBUG_OUT_TIMING
	#define SUART_DEBUG_OUT_TIMING_ON_ASM  "sbi 0x18,0\n\t"
	#define SUART_DEBUG_OUT_TIMING_OFF_ASM "cbi 0x18,0\n\t"
	#define SUART_DEBUG_OUT_TIMING_ON()   __asm__ ("sbi 0x18,0")
	#define SUART_DEBUG_OUT_TIMING_OFF()  __asm__ ("cbi 0x18,0")
#else
	#define SUART_DEBUG_OUT_TIMING_ON_ASM  
	#define SUART_DEBUG_OUT_TIMING_OFF_ASM 
	#define SUART_DEBUG_OUT_TIMING_ON()
	#define SUART_DEBUG_OUT_TIMING_OFF()
#endif
//------------------------------------------------------
#ifdef SUART_DEBUG_IN_TIMING
	#define SUART_DEBUG_IN_TIMING_ON_ASM 		"sbi 0x18,1\n\t"
	#define SUART_DEBUG_IN_TIMING_OFF_ASM		"cbi 0x18,1\n\t"
	#define SUART_DEBUG_IN_TIMING_ON()   __asm__ ("sbi 0x18,1")
	#define SUART_DEBUG_IN_TIMING_OFF()  __asm__ ("cbi 0x18,1")
#else
	#define SUART_DEBUG_IN_TIMING_ON_ASM  
	#define SUART_DEBUG_IN_TIMING_OFF_ASM 
	#define SUART_DEBUG_IN_TIMING_ON() 
	#define SUART_DEBUG_IN_TIMING_OFF()
#endif
//------------------------------------------------------
#endif // _SOFT_UART_H
//------------------------------------------------------
